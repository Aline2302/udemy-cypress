/// <reference types="cypress" />

it("Escolhendo a opção do Select", () => {
  cy.visit('?id_category=5&controller=category')
    .get('#selectProductSort')
    .select('name:asc')

})

it("Escolhendo Multiplos opções do Select", () => {
  cy.visit('http://slimselectjs.com/?p=%2Fmethods%2')
    .get('#slim-multi-select')
    .select(['Best', 'Ever'], { force: true })
  //ex:.select('Best', { force: true })
})
