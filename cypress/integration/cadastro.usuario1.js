/// <reference types="cypress" />

const { faker } = require('@faker-js/faker')

describe('cadastro de usuário', () => {
  let user

  before(() => {
    user = {
      email: faker.internet.email(),
      name: {
        first: faker.name.firstName(),
        last: faker.name.lastName()
      }
    }
    cy.visit('?controller=authentication&back=my-account')

  })

  it('Informar email novo', () => {
    cy.get('#email_create').type(`${user.email}{enter}`)

  })

  it('Preencher campos obrigatorios', () => {
    cy.url().should('include', '#account-creation')
    //cy.get('#account-creation_form > :nth-child(1) > .clearfix > :nth-child(1)').should('be.visible')
    cy.get('#email').should('have.value', user.email)
    cy.get('#email').invoke('val').then((element) => {
      expect(element).eq(user.email)
    })
    cy.get('#id_gender1').check()
    cy.get('#customer_firstname').type(user.name.first)
    cy.get('#customer_lastname').type(user.name.last)
    cy.get('#passwd').type(faker.internet.password())
    cy.get('#address1').type(faker.address.streetAddress())
    cy.get('#city').type(faker.address.cityName())
    cy.get('#id_state').select(`${faker.datatype.number({ min: 1, max: 20 })}`)
    cy.get('#postcode').type(`${faker.datatype.number({ min: 10000, max: 99999 })}`)
    cy.get('#phone_mobile').type(faker.phone.phoneNumberFormat())

  })
  it('Finalizar cadastro', () => {
    cy.get('#submitAccount > span').click()
    cy.get('.account > span').should('have.text', `${user.name.first} ${user.name.last}`)
  })

})
