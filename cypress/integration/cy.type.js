/// <reference types="cypress" />

//const { type } = require('cypress/types/jquery')
//{backspace}{del}{downarrow} {end}{enter}{esc}{home}{insert}{leftarrow}{movetoend}{movetostart}{pagedown}
//{pageup}{rightarrow}{selectall}{uparrow}

//ex:it("Digirando em Elementos", () => {
//cy.visit('?id_category=5&controller=category')
//.get('#search_query_top')
//.type('Dress')
//.get('#searchbox > .btn')
//.click()


//ex:.type('{selectall}Red{enter}')
//ex:.type('{movetostart}Red {enter}')


it("Digirando em Elementos", () => {
  let itemCategory = "Dress"
  let itemDescription = "Summer"
  cy.visit('?id_category=5&controller=category')
    .get('#search_query_top')
    .type(`${itemCategory} ${itemDescription}`)
    .get('#searchbox > .btn')
    .click()

})
