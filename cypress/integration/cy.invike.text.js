/// <reference types="cypress" />

it("cy.get() -Selecionando Elementos", () => {
    cy.visit('?id_category=5&controller=category')
        .get('.shop-phone >strong')
        .invoke('text')
        .should('equal', '0123-456-789')

})
it("cy.get() -Selecionando Elementos", () => {
    cy.visit('?id_category=5&controller=category')
        .get('.right-block > .content_price > .price')
        .invoke('text')
        .should('equal', '$16.51')

})