/// <reference types="cypress" />

it("cy.get() -Selecionando Elementos", () => {
    cy.visit('?id_category=5&controller=category')
        .get('#newsletter-input')
        .invoke('val')
        .should('equal', 'Enter your e-mail')
})