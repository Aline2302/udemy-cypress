/// <reference types="cypress" />

it("cy.get() -Selecionando Elementos", () => {
    cy.visit('?id_category=5&controller=category')
        .get('.right-block > .content_price > .price')
        .then((preco) => {
            let textFormato = preco.text().replace('\t', '').replace('\n', '').trim()
            expect(textFormato).to.equal('$16.51')
        })
})

