/// <reference types="cypress" />

//const { find } = require('cypress/types/lodash')

//const { get } = require('cypress/types/lodash')

it("Escolhendo uma opção do Checkbox", () => {
  cy.visit('?id_category=5&controller=category')
    .get("#ul_layered_id_attribute_group_1")
    .find("[type=checkbox]")
    .each((checkbox) => {
      cy.get(checkbox)
        .check()
    })
})

it("Escolhendo uma opção do Radio", () => {
  cy.visit('?controller=authentication&back=my-account')
    .get('#email_create')
    .type("email_aline2302@live.com{enter}")
    .get('#id_gender2')
    .check()


  //ex:.get('#layered_id_attribute_group_1')
  //.check()
  //.get('#layered_id_attribute_group_2')
  //.check()
  //.get('#layered_id_attribute_group_3')
  //.check()
  //.uncheck()

})

