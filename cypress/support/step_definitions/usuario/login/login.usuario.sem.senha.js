/// <reference types="cypress" />

/* global Then, When, Given */

When('preencho os dados de login sem informar senha',()=> {
    global.user.email = 'Daphnee_Balistreri79@hotmail.com'
    let backupsenha = global.user.senha
    global.user.senha =' '
    
    cy.get('#email').type('global.user.email')
    cy.get('#passwd').type('global.user.senha').then(()=>{global.user.senha = backupsenha})
})

Then('o sistema notifica usuario que e necessario informar uma senha',()=>{
    cy.get('div.alert.alert-danger').should(be.visible)
    find('ol > li').should('have.text','Password is required.')
})
//usuario:Daphnee_Balistreri79@hotmail.com
//senha:oBff3CytPaC0RhR