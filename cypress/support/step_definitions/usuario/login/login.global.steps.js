/// <reference types="cypress" />

const AuthenticationPage = require('../../../pages/authentication.page')

/* global Then, When, Given */


Then('o sistema realiza meu login com sucesso me autenticando na pagina',()=> {
    cy.get('.account > span')
    .should('be.visible')
    .should('have.text',global.user.name)
})

Then('o sistema notifica usuario que houve problema com a autenticacao',()=>{
cy.get('div.alert.alert-danger').should(be.visible)
find('ol > li').should('have.text','Authentication failed.')
})

And('clico para realizar login',()=>{
    AuthenticationPage.clicaRealizarLogin()
}) 
//usuario:Daphnee_Balistreri79@hotmail.com
//senha:oBff3CytPaC0RhR