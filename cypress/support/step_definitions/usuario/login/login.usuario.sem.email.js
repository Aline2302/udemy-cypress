/// <reference types="cypress" />

/* global Then, When, Given */

When('preencho os dados de login sem informar email',()=> {
    global.user.email = ' '
    
    cy.get('#email').type('global.user.email')
    cy.get('#passwd').type('global.user.senha')
})
Then('o sistema notifica usuario que e necessario informar um email',()=>{
    cy.get('div.alert.alert-danger').should(be.visible)
    find('ol > li').should('have.text','An email address required.')
    
})


//usuario:Daphnee_Balistreri79@hotmail.com
//senha:oBff3CytPaC0RhR