import AuthenticationElements from '../elements/authentication.elements'

const authenticationPage = require('../elements/authentication.elements')

export default class AuthenticationPage { 
static cliqueAcessarAuthentication(){
    cy.visit('').then(()=>{
        AuthenticationElements.btnSinIn().click()
        .url().should('include','?controller=authentication&back=my-account')
    })
}
static digitaEmailLogin(){
  return AuthenticationElements.inputEmailLogin().type(global.user.email)
}

static digitasenhaLogin(){
   return AuthenticationElements.inputsenhaLogin().type(global.user.senha)
}

static preencheDadosLogin(){
    return this.digitaEmailLogin().then(()=>{
        this.digitasenhaLogin()
    })
}
static clicaRealizarLogin(){
   
    return AuthenticationElements.btnLogin().click()
}

static verificaUsuarioAuthenticado(){
    return AuthenticationElements.labelUsuarioAutenticado()
     .should('be.visible')
     .should('have.text',global.user.name)
}


}